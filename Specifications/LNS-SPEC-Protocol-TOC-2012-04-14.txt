LOCATION NAME SERVICE

Table of Contents

A. VOCABULARY

A. 1. Mobile
A. 2. Server
A. 3. Device
A. 4. Action
A. 5. Agency
A. 6. Ask
A. 7. Bid
A. 8. Match

B. VARIABLES

B. 1. Mobile
B. 2. Server
B. 2. a. Current Time Server <-> Mobile/Device
B. 2. b. LNS Connection Server <-> Mobile
B. 2. c. LNS Connection Server <-> Device
B. 2. d. LNS Input Handler Server Process
B. 2. e. LNS Output Handler Server Process
B. 2. f. LNS Input Server Process Logger
B. 2. g. LNS Output Server Process Logger
B. 3. Device
B. 4. Action
B. 5. Agency
B. 6. Ask
B. 7. Bid
B. 8. MATCH

C. PROTOCOL

C. 1: LOCATION NAME SERVICE SESSION		Mobile -> Server
C. 2: LOCATION NAME SERVICE DISCOVERY		Server -> Mobile
C. 3: LOCATION NAME SERVICE NEGOTIATION		Mobile -> Server
C. 4: LOCATION NAME SERVICE AVAILABILITY	Server -> Mobile
C. 5: LOCATION NAME SERVICE RESERVATION		Mobile -> Server
C. 6: LOCATION NAME SERVICE ACCEPTANCE		Server -> Mobile
C. 7: LOCATION NAME SERVICE DELIVERY		Server -> Device
C. 8: LOCATION NAME SERVICE RECEIPT		Device -> Server