Location Name Service (LNS) DRAFT 2012-04-14 16:55

A. VOCABULARY

A. 1. Mobile

      A physical, portable computer such as a smart mobile phone that
      can send a virtual or current physical Location (Latitude,
      Longitude, Altitude) and Cardinal Direction via a defined
      protocol over HTTP/1.1 in a TCP/IP-based network connection
      to an LNS Server.

A. 2. LNS Server

      A logical server computer such as a "1U rack" with CPU, RAM and
      disk, virtual memory, a process that acts on input variables and
      processes this information in virtual memory, and a process that
      returns information to a Device when the input parameters
      triggers an Action based on the logical rules programmed by an
      Agency.

A. 3. Device

      A physical sensor output device such as a television screen,
      audio speaker, or an input device such as a video or audio
      recoder that receives information from a Server and that
      executes an Action that is recognisable by the Mobile.

A. 4. Action : An event that is triggered by pre-programmed rules that
               is based on input sensor parameters from a Mobile sent
               to a Server over TCP/IP network, processed by the
               Server and sent to a Device over TCP/IP network to
               execute something that is recognisable to the Mobile.

A. 5. Agency : Auto-algorithm writer or programmers that is using the
               System to write programs that uses the elements in the
               System to trigger Action on a Device recognized by the
               Mobile.
             
A. 6. Seller : A provider of material, virtual or gratis products
               available for sale in the moment when a Action was
               triggered on a Device to a Mobile.
 
A. 7. Bidder : A bidder of an Action that triggers an Action on a
               Device.

A. 8. Winner : Occurs when an Agency has programmed a rule that
               triggers an Action for a Seller by Bidder on a Device
               recognizable to Mobile that agrees to buy from the
               Seller.

B. VARIABLES

B. 1. Mobile

   The primary input sensor moving in space, direction and time and
   that provides sensor output parameters to the Server such as

   Output Variables to Server.

   : IP Address
     Example output values: ("129.1.178.188")

   : Session ID
     Example output values: ("e1ec37ec-b326-464d-8993-cbc9bf657ce4")

   : GPS Latitude
     Example output values: ("10.362")

   : GPS Longitude 
     Example output values: ("32.102")

   : GPS Altitude
     Example output values: ("0", "5m", "10m", "15m", "500m", "3000m", "10000m")
 
   : Cardinal direction Example output values:
     ("North","NorthEast","NorthWest","South", "SouthEeast",
     "SouthWest", "E", "W")

B. 2. Server
  
   The Server is the input request and response engine that processes input 
   sensor parameters from Mobile and outputs a response to the Device based
   on logical Action event triggers programmed by the Agency, and sends the
   Action event trigger to the Device.
   
B. 2. a. Current Time

      The System Time is in UTC and every Server request and Server response is logged.

B. 2. b. TCP/IP Connection Server <-> Mobile

         Example: 

         Proto Recv-Q Send-Q Local Address         Foreign Address             State      
         tcp        0      0 lns-server:80         129.01-178-188.mobile:62549 ESTABLISHED

B. 2. c. TCP/IP Connection Server <-> Device
 
         Example:

         Proto Recv-Q Send-Q Local Address         Foreign Address             State      
         tcp        0      0 lns-server:80         211.90-149-168.device:62549 ESTABLISHED

B. 2. d. Input Handler Process ID

         Example:

         lnsd       2670  0.0  0.1  24616  5132 pts/1    Ss   23:31   0:00 lns-mobile-handler

B. 2. e. Output Handler Process ID

      	 Example:

         lnsd       2671  0.0  0.1  24616  5132 pts/1    Ss   23:31   0:00 lns-device-handler

B. 2. f. Input Process Logger

         The Server Input Process logs incoming input sensor from a
         Mobile parameters and the Actions.

         The Server will use pre-computed approximation tables such as the following.

	 Example:

         Apr 13 22:40:55 lns-mobile-handler: Input("129.1.178.188,10.362,32.102,5,NW)

         : physical distance between Mobile and Device
         : cardinal direction ("North","South","East","West") between Mobile and Device

B. 2. g. Output Process Logger
 
         Apr 13 22:40:55 lns-device-handler
	 Apr 13 22:40:55 Action "Carl_Berner_LCD_42_NW" "15s" "htc-one-x-NO.avi"

C. Device

   The Device is the output sensor that provides an Action
   recognizable to the Mobile based on input parameters processed by
   the Server by pre-programmed logic based on 

	latitude, longitude, compass direction, distance between mobile and device

   The Device is the output sensor in the system.

   The Device can be an audio/video output device or input device

D. ACTION

   The Action is measured in seconds and is the information provided on the Device if the
   Mobile is near and the Mobile has approved that about the Mobile

   latitude, longitude, compass direction

E. AGENCY

   The agency is the programmed system operators that returns output sensors to the device
   based on input sensor parameters from the mobile

F. SELLER

G. BIDDER

H. WINNER

I. PROTOCOL

   1. Virtual Location	MAP COORDINATES

   2. Current Location	GPS COORDINATES

I. A: LOCATION NAME SERVICE SESSION

   Mobile starts a LNS/1.0 protocol sesssion to the Server over HTTP/1.1

I. A. 1. LNS/1.0 INITIAL SESSION

      GET http://lns.geopher.com/initial?sessionid=7432c77d-ba07-4e1c-85a4-9c8ae0e6bcf7&lat=62.12345&lng=15.12345 HTTP/1.1

		GET Variables
		SessionID	7432c77d-ba07-4e1c-85a4-9c8ae0e6bcf7
	     	Latitude	62.12345
	     	Longitude	15.12345

      CATCH Invalid Session:	

                Handle_Session_Error();

I. A. 2. LNS/1.0 INITIAL SESSION

      GET http://lns.geopher.com/initial?sessionid=e52784e4-2b5b-4f2e-8b20-de21262eb21b&lat=62&lng=15 HTTP/1.1

        	GET Variables
	     	Latitude	62
		Longitude	15

      CATCH Invalid Session:

                Handle_Session_Error();

I. B: LOCATION NAME SERVICE NEGOTIATION

I. B. 1. LNS/1.0 NEGOTIATE

      GET http://lns.geopher.com/negotiate?Latitude=62.12345&Longitude=15.12345

		GET Variables
		Budget		100USD
		Target		public

      CATCH Invalid Negotiation:
      
		Handle_Negotiation_Error();

I. B. 2. LNS/1.0 NEGOTIATE

      GET http://lns.geopher.com/negotiate?Latitude=62&Longitude=15

      CATCH Invalid Negotiation:
      
		Handle_Negotiation_Error();

I. C: LOCATION NAME SERVICE DISCOVERY

I. C: 1. LNS/1.0 DISCOVERY

      GET http://lns.geopher.com/discovery?

		GET Variables

       LOCATION	       NAME            DEVICE  INTENT	 RESOLUTION  PRICE  PERIOD  DURATION
   #   62.0001,15.0001 dno_hall_screen screen  commerce	 1080p	     
   #   62.0002,15.0002 dno_hall_cinema cinema  public	  720p      		
   #   10.0000,10.0000 kalles_phone    phone   private	  720p       1 USD
   #   20.0000,20.0000 vg.no           website commerce	 1080p
   #                   army.mil        screen  military  1080p
   #                   whitehouse.gov  screen  goverment 1080p

D: LOCATION NAME SERVICE AVAILABILITY

   1. LNS/1.0 AVAILABLE

      GET http://lns.geopher.com/available/?name=dno_hall_screen

		GET Variables

   2. LNS/1.0 AVAILABLE

   
   #   62.0000,15.0000 dno_taket         AVAILABLE	TICKET		SPACE
   #   62.0011,15.0019 dno_rad_1_plass_9 AVAILABLE      TICKET		SEAT
   #   62.0000,15.0000 dno_hall          AVAILABLE	TICKET		EVENT
   #   62.0010,15.0020 dno_reception     AVAILABLE	TICKET		JOB

E: LOCATION NAME SERVICE RESERVATION

   1. LNS/1.0 RESERVE

      GET http//lns.geopher.com/reserve

   2. LNS/1.0 RESERVE

      