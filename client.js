document.addEventListener('DOMContentLoaded', async () => {
    try {
        const response = await axios.get('/fetch-data');
        document.getElementById('data').textContent = JSON.stringify(response.data, null, 2);
    } catch (error) {
        console.error('Error fetching data:', error.message);
    }
});
