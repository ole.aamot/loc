<!DOCTYPE html>
<html>
  <head>
    <title>Simple Map</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map {
        height: 100%;
      }
    </style>
  </head>
  <body>
<form method="GET" action="/">
<input type="text" name="name" placeholder="Name" value="<?php echo $_GET['name']; ?>" />
<input type="text" name="glat" placeholder="gLat" value="<?php echo $_GET['glat']; ?>" />
<input type="text" name="glon" placeholder="gLon" value="<?php echo $_GET['glon']; ?>" />
<input type="text" name="gurl" placeholder="gURL" value="<?php echo $_SERVER['REQUEST_URI']; ?>" />
<input type="submit" name="submit" value="Map This" />
</form>
    <div id="map"></div>
        <script>

      // This example adds a marker to indicate the position of Bondi Beach in Sydney,
      // Australia.
      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 4,
          center: {lat: <?php echo $_GET['glat']; ?>, lng: <?php echo $_GET['glon']; ?>}
        });

        var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
        var beachMarker = new google.maps.Marker({
          position: {lat: <?php echo $_GET['glat']; ?>, lng: <?php echo $_GET['glon']; ?>},
	  label: { text: '<?php echo $_GET['name'];?>'},
	  map: map,
          icon: image
        });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDLPt4vzuvmFOOpmhxkcxBIC1qpdKIJowo&callback=initMap">
    </script>
  </body>
</html>
