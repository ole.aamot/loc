const express = require('express');
const fetch = require('node-fetch');

const app = express();
const port = 3000;

app.use(express.static('public'));
app.use(express.json());

app.get('/fetch-data', async (req, res) => {
    const { service, glat, glon, radius } = req.query;

    try {
        const response = await fetch(`https://api.piperpal.com/location/json.php?service=${service}&glat=${glat}&glon=${glon}&radius=${radius}`);
        const data = await response.json();
        res.json(data);
    } catch (error) {
        res.status(500).json({ error: 'Error fetching data' });
    }
});

app.listen(port, () => {
    console.log(`Server listening on port ${port}`);
});
